package com.example.webTesting;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.webTesting.dao.TodoNoteMapper22;

@SpringBootTest
public class TestDataSources {

	
	@Autowired
	TodoNoteMapper22 mapper;
	
	
	@Test
	public void test() {
//		mapper.getAllList();
		assertThat(mapper).isNotNull();
	}

}
