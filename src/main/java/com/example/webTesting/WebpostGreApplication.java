package com.example.webTesting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebpostGreApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebpostGreApplication.class, args);
	}

}
