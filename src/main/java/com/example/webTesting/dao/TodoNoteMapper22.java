package com.example.webTesting.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;



@Mapper
public interface TodoNoteMapper22 {
	
	  @Select("SELECT * FROM notes")
	  public List<TodoNote> getAllList();

	  
	  @Select("SELECT * FROM notes WHERE uid = ${uid}")
	  public TodoNote selectOnce(int uid);
	  
	  
	  @Select("SELECT count(*) FROM notes")
	  int getCount();
	  
	  @Update("UPDATE notes SET body = #{body} WHERE uid = #{uid}")
	  @Options(useGeneratedKeys = true, keyProperty = "uid")
	  int update(TodoNote todoNote);
	  
	  @Insert("INSERT INTO notes (body) VALUES(#{body})")
	  @Options(useGeneratedKeys = true, keyProperty = "uid")
	  Integer insert(TodoNote body);
	  
	  @Delete("Delete From notes WHERE uid = #{uid}")
	  int delete(int uid);
	  
}


