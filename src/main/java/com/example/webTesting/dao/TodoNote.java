package com.example.webTesting.dao;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TodoNote {
	
	public TodoNote() {
		
	}
	
	 int uid;
	 String body;
}
