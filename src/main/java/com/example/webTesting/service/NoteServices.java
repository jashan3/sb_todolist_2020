package com.example.webTesting.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.webTesting.dao.TodoNote;
import com.example.webTesting.dao.TodoNoteMapper22;

@Service
public class NoteServices {

	@Autowired
	TodoNoteMapper22 mapper;
	
	public List<TodoNote> getTodoNoteList(){
		return mapper.getAllList();
	}
	
	public TodoNote getTodoNote(int uid) {
		return mapper.selectOnce(uid);
	}
	
	public int getTodoNoteCount(){
		return mapper.getCount();
	}
	
	public int updateTodo(TodoNote body) {
		return mapper.update(body);
	}
	
	public Integer insertTodo(TodoNote body) {
		return mapper.insert(body);
	}
	public int delete(int uid) {
		return mapper.delete(uid);
	}
}
