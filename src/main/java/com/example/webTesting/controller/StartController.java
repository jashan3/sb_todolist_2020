package com.example.webTesting.controller;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.webTesting.dao.TodoNote;
import com.example.webTesting.dao.TodoNoteMapper22;
import com.example.webTesting.service.NoteServices;

import io.swagger.annotations.ApiOperation;


@Controller
public class StartController {

	private Logger logger = LoggerFactory.getLogger(StartController.class);
	
	@Autowired
	NoteServices noteService;
	
	
	@RequestMapping("/home")
	public String ping() {
		return "todolist";
	}
	
 

	@ApiOperation(value = "모든 todoList 항목", notes="모든 todoList 항목을 볼수있다.")
	@RequestMapping(value = "/notes/getAll", method = RequestMethod.GET) 
	@ResponseBody
	public ResponseEntity<List<TodoNote>> getAllNotes() {
		logger.info("==========>");
		logger.info("/notes/getAll");
		List<TodoNote> list = noteService.getTodoNoteList();
		if (list != null) {
			logger.info("list: "+list.toString());
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
		logger.info("<==========");
		return ResponseEntity.status(HttpStatus.OK).body(list);
	}
	
	
	@ApiOperation(value = "todoList의 갯수", notes="현재 todoList의 갯수를 알 수 있다.")
	@RequestMapping(value = "/notes/count", method = RequestMethod.GET) 
	@ResponseBody
	public ResponseEntity<Integer> getCountNotes() {
		logger.info("==========>");
		logger.info("/notes/getAll");
		Integer count = noteService.getTodoNoteCount();
		logger.info("list: "+count);
		logger.info("<==========");
		return ResponseEntity.status(HttpStatus.OK).body(count);
	}
	
	
	@ApiOperation(value = "todoList 수정", notes="작성한 todoList를 수정한다.")
	@RequestMapping(value = "/notes/update", method = RequestMethod.POST) 
	@ResponseBody
	public ResponseEntity<TodoNote> updateNote(@RequestBody TodoNote note) {
		logger.info("==========>");
		logger.info("/notes/update");
		if (note != null) {
			int myID = note.getUid();
			String body = note.getBody();
			TodoNote noted = new TodoNote(myID,body);
			int skey = noteService.updateTodo(noted);
			logger.info("skey: "+noted.getUid());
			return ResponseEntity.status(HttpStatus.OK).body(noted);
		}
		
		logger.info("<==========");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
	}
	
	@ApiOperation(value = "todoList 작성", notes="작성한 todoList DB에 넣는다.")
	@RequestMapping(value = "/notes/insert", method = RequestMethod.POST) 
	@ResponseBody
	public ResponseEntity<TodoNote> insertNote(@RequestBody TodoNote note) {
		logger.info("==========>");
		logger.info("/notes/insert");
		if (note != null) {
			int myID = note.getUid();
			String body = note.getBody();
			TodoNote noted = new TodoNote(10000,body);
			int skey = noteService.insertTodo(noted);
			logger.info("skey: "+noted.getUid());
			return ResponseEntity.status(HttpStatus.OK).body(noted);
		}
		
		logger.info("<==========");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
	}
	
	
	@ApiOperation(value = "todoList 제거", notes="작성한 todoList DB에서 제거한다.")
	@RequestMapping(value = "/notes/delete", method = RequestMethod.POST) 
	@ResponseBody
	public ResponseEntity<TodoNote> deleteNote(@RequestBody int key) {
		logger.info("==========>");
		logger.info("/notes/delete"+key);
		TodoNote note = noteService.getTodoNote(key);
		if (note == null) {
			logger.info("note == null");
		} else {
			logger.info("note: "+note.getBody());
			logger.info("note: "+note.getUid());
			noteService.delete(key);
			return ResponseEntity.status(HttpStatus.OK).body(note);
		}
		
		logger.info("<==========");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
	}
	
	

	@ApiOperation(value = "todoList 한 항목 불러오기", notes="작성한 todoList DB에서 한개를 불러온다.")
	@RequestMapping(value = "/notes/oneNote", method = RequestMethod.POST) 
	@ResponseBody
	public ResponseEntity<TodoNote> getOnceNote(@RequestBody int key) {
		logger.info("==========>");
		logger.info("/notes/oneNote"+key);
		TodoNote note = noteService.getTodoNote(key);
		if (note == null) {
			logger.info("note == null");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
		
		logger.info("<==========");
		return ResponseEntity.status(HttpStatus.OK).body(note);
	}
}
